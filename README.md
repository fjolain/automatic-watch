## v3 : Watch case for Miyota 8205

Wear the most unique and cheapest automatic watch on your wrist !
![Image](v3/image.jpeg)

To do so you will need some purchasses:

* [Miyota 8205](https://www.amazon.fr/MagiDeal-Movement-Automatique-M%C3%A9canique-Miyota/dp/B073ZZMS5T/ref=sr_1_fkmr0_2?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=24HMVKW6WYW1Y&dchild=1&keywords=miyota+8205&qid=1593963453&sprefix=miyo%2Caps%2C144&sr=8-2-fkmr0)
* [18 mm NATO strap](https://www.amazon.fr/Gemony-Bracelet-Montre-Ballistic-WB-100/dp/B01GYI2AQ8/ref=sr_1_5?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2O8V5ZQGXZMHN&dchild=1&keywords=nato+strap+18mm&qid=1593963506&sprefix=nato+strap+18%2Caps%2C151&sr=8-5)
* [glass 1 x 26 mm](https://www.amazon.fr/Verre-optique-rechange-montre-epaisseur/dp/B01BQ7XVYW/ref=sr_1_7?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=verre+montre+26mm&qid=1593963587&sr=8-7)

## v4 : Apple Watch (with 2824-2 ETA)
![Image](v4/Image 2.jpeg)
![Image](v4/Image 3.jpeg)

PS: Printed model in picture was made with [Zortrax M200 printer](https://zortrax.com/3d-prin)
